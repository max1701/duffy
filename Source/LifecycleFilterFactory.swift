/**
    ...
*/
class LifecycleFilterFactory : FilterFactory {

    /**
        ...
    */
    private class CachedInstance {

        let type: Any.Type
        let tag: String?
        let instance: Any?

        init(type: Any.Type, tag: String?, instance: Any?) {
            self.type = type
            self.tag = tag
            self.instance = instance
        }
    }

    /**
        ...
    */
    private class SingletonFilter : Filter {

        private var cachedInstances: [CachedInstance]
        private let type: Any.Type
        private let tag: String?

        private init(cachedInstances: [CachedInstance], type: Any.Type, tag: String?) {
            self.cachedInstances = cachedInstances
            self.type = type
            self.tag = tag
        }

        func applyToFactory(factory: () -> Any?) -> () -> Any? {
            return {
                let matches = self.cachedInstances.filter { $0.type == self.type && $0.tag  == self.tag }
                if matches.isEmpty {
                    let instance = factory()
                    self.cachedInstances.append(CachedInstance(type: self.type, tag: self.tag, instance: instance))
                    return instance
                } else {
                    // potentially, more than one elements where found by the provided matcher,
                    // so just return the first in the list
                    return matches[0].instance
                }
            }
        }
    }

    /**
        ...
    */
    private class TransientFilter : Filter {
        func applyToFactory(factory: () -> Any?) -> () -> Any? {
            return factory // just pass through
        }
    }

    private let cachedInstances: [CachedInstance]

    init() {
        cachedInstances = []
    }

    func filterConfiguredWithType(type: Any.Type, tag: String?, lifecycle: Lifecycle, dependencies: [Any.Type]?) -> Filter {
        switch lifecycle {
            case .Singleton: return SingletonFilter(cachedInstances: cachedInstances, type: type, tag: tag)
            case .Transient: return TransientFilter()
        }
    }
}
