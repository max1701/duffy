/**
    ...
*/
class InstanceFactory {

    let type: Any.Type
    let tag: String?
    let lifecycle: Lifecycle
    let dependencies: [Any.Type]?
    let factory: () -> Any?

    init(type: Any.Type, tag: String?, lifecycle: Lifecycle, dependencies: [Any.Type]?, factory: () -> Any?) {
        self.type = type
        self.tag = tag
        self.lifecycle = lifecycle
        self.dependencies = dependencies
        self.factory = factory
    }

}
