/**
    ...
*/
protocol FilterFactory {

    /**
        ...
    */
    func filterConfiguredWithType(type: Any.Type, tag: String?, lifecycle: Lifecycle, dependencies: [Any.Type]?) -> Filter
}