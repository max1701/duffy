/**
    ...
*/
public enum Lifecycle {
    case Singleton
    case Transient
}