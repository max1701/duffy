/**
    ...
*/
public class Container {

    let filterFactory: FilterFactory
    var instanceFactories: [InstanceFactory]

    init() {
        instanceFactories = []
        filterFactory = ChainingFilterFactory(factories:
            LifecycleFilterFactory()
            // add filter factories here ...
        )
    }

    /**
        Lorem ipsum ...
        
        ```swift
        println()
        ```
        - parameter loaderFunc: Lorem ipsum ...
    */
    public func loadUsing(loaderFunc: (Loader) -> Void) {
        loaderFunc(Loader(container: self))
    }

    /**
        Lorem ipsum ...
    
        ```swift
        println()
        ```
        - parameter loaderFunc: Lorem ipsum ...
        - returns: the container instance (self)
    */
    public func loadedBy(loaderFunc: (Loader) -> Void) -> Container {
        loadUsing(loaderFunc)
        return self
    }

    /**
        ...
    */
    public func instanceOfType<T>(type: T.Type) -> T? {
        return instanceOfType(type, taggedAs: nil)
    }

    /**
        ...
    */
    public func instanceOfType<T>(type: T.Type, taggedAs tag: String) -> T? {
        return instanceOfType(type, taggedAs: Optional(tag))
    }

    /**
        ...
    */
    public func instanceOfType<T>(type: T.Type, taggedAs tag: CustomStringConvertible) -> T? {
        return instanceOfType(type, taggedAs: Optional(tag.description))
    }

    /**
        ...
    */
    func loadWithType(type: Any.Type, taggedAs tag: String?, usingLifecycle lifecycle: Lifecycle, dependingOn dependencies: [Any.Type]?, usingFactory factory: () -> Any?) -> Container {

        // first, feed the factory filter chain with the provided instance-factory 'nucleus'
        let filteredFactory = filterFactory
            .filterConfiguredWithType(type, tag: tag, lifecycle: lifecycle, dependencies: dependencies)
            .applyToFactory(factory)

        // finally, add the filtered instance factory to the list of factories
        instanceFactories.append(InstanceFactory(type: type, tag: tag, lifecycle: lifecycle, dependencies: dependencies, factory: filteredFactory))

        return self
    }

    /**
        ...
    */
    func instanceOfType<T>(type: T.Type, taggedAs tag: String?) -> T? {
        var found: [InstanceFactory] = []
        if let tag = tag {
            found += instanceFactories.filter {$0.type == type && $0.tag == tag}
        } else {
            found += instanceFactories.filter {$0.type == type && $0.tag == nil}
            found += instanceFactories.filter {$0.type == type && $0.tag != nil}   // fallback, if no factory w/o tag is found, use any tagged of the same type !
        }
        return !found.isEmpty ? found[0].factory() as? T : nil
    }

}

