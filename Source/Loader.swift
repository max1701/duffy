/**
    ...
*/
public class Loader {

    let DEFAULT_LIFECYCLE = Lifecycle.Transient

    let container: Container

    init(container: Container) {
        self.container = container
    }

    /**
        ...
    */
    public func loadWithType<T>(type: T.Type, usingFactory factory: () -> T) -> Loader {
        return loadWithType(type, usingLifecycle: DEFAULT_LIFECYCLE, usingFactory: factory)
    }

    /**
        ...
    */
    public func loadWithType<T>(type: T.Type, usingLifecycle lifecycle: Lifecycle, usingFactory factory: () -> T) -> Loader {
        container.loadWithType(type, taggedAs: nil, usingLifecycle: lifecycle, dependingOn: nil) {
            return factory()
        }
        return self
    }

    /**
        ...
    */
    public func loadWithType<T, D>(type: T.Type, dependingOn dependency: D.Type, usingFactory factory: (D) -> T) -> Loader {
        return loadWithType(type, usingLifecycle: DEFAULT_LIFECYCLE, dependingOn: dependency, usingFactory: factory)
    }

    /**
        ...
    */
    public func loadWithType<T, D>(type: T.Type, usingLifecycle lifecycle: Lifecycle, dependingOn dependency: D.Type, usingFactory factory: (D) -> T) -> Loader {
        container.loadWithType(type, taggedAs:  nil, usingLifecycle: lifecycle, dependingOn: [dependency]) {
            let d: D? = self.container.instanceOfType(dependency)
            return d != nil ? factory(d!) : nil
        }
        return self
    }

    /**
        ...
    */
    public func loadWithType<T, D1, D2>(type: T.Type, dependingOn dependency1: D1.Type, and dependency2: D2.Type, usingFactory factory: (D1, D2) -> T) -> Loader {
        return loadWithType(type, usingLifecycle: DEFAULT_LIFECYCLE, dependingOn: dependency1, and: dependency2, usingFactory: factory)
    }

    /**
        ...
    */
    public func loadWithType<T, D1, D2>(type: T.Type, usingLifecycle lifecycle: Lifecycle, dependingOn dependency1: D1.Type, and dependency2: D2.Type, usingFactory factory: (D1, D2) -> T) -> Loader {
        container.loadWithType(type, taggedAs:  nil, usingLifecycle: lifecycle, dependingOn: [dependency1, dependency2]) {
            let d1: D1? = self.container.instanceOfType(dependency1)
            let d2: D2? = self.container.instanceOfType(dependency2)
            return d1 != nil && d2 != nil ? factory(d1!, d2!) : nil
        }
        return self
    }

    /**
        ...
    */
    public func loadWithType<T, D1, D2, D3>(type: T.Type, dependingOn dependency1: D1.Type, and dependency2: D2.Type, and dependency3: D3.Type, usingFactory factory: (D1, D2, D3) -> T) -> Loader {
        return loadWithType(type, usingLifecycle: DEFAULT_LIFECYCLE, dependingOn: dependency1, and: dependency2, and: dependency3, usingFactory: factory)
    }

    /**
        ...
    */
    public func loadWithType<T, D1, D2, D3>(type: T.Type, usingLifecycle lifecycle: Lifecycle, dependingOn dependency1: D1.Type, and dependency2: D2.Type, and dependency3: D3.Type, usingFactory factory: (D1, D2, D3) -> T) -> Loader {
        container.loadWithType(type, taggedAs: nil, usingLifecycle: lifecycle, dependingOn: [dependency1, dependency2, dependency3]) {
            let d1: D1? = self.container.instanceOfType(dependency1)
            let d2: D2? = self.container.instanceOfType(dependency2)
            let d3: D3? = self.container.instanceOfType(dependency3)
            return d1 != nil && d2 != nil && d3 != nil ? factory(d1!, d2!, d3!) : nil
        }
        return self
    }

    /**
        ...
    */
    public func loadWithType<T, D1, D2, D3, D4>(type: T.Type, dependingOn dependency1: D1.Type, and dependency2: D2.Type, and dependency3: D3.Type, and dependency4: D4.Type, usingFactory factory: (D1, D2, D3, D4) -> T) -> Loader {
        return loadWithType(type, usingLifecycle: DEFAULT_LIFECYCLE, dependingOn: dependency1, and: dependency2, and: dependency3, and: dependency4, usingFactory: factory)
    }

    /**
        ...
    */
    public func loadWithType<T, D1, D2, D3, D4>(type: T.Type, usingLifecycle lifecycle: Lifecycle, dependingOn dependency1: D1.Type, and dependency2: D2.Type, and dependency3: D3.Type, and dependency4: D4.Type, usingFactory factory: (D1, D2, D3, D4) -> T) -> Loader {
        container.loadWithType(type, taggedAs: nil, usingLifecycle: lifecycle, dependingOn: [dependency1, dependency2, dependency3, dependency4]) {
            let d1: D1? = self.container.instanceOfType(dependency1)
            let d2: D2? = self.container.instanceOfType(dependency2)
            let d3: D3? = self.container.instanceOfType(dependency3)
            let d4: D4? = self.container.instanceOfType(dependency4)
            return d1 != nil && d2 != nil && d3 != nil && d4 != nil ? factory(d1!, d2!, d3!, d4!) : nil
        }
        return self
    }

    /**
        ...
    */
    public func loadWithType<T, D1, D2, D3, D4, D5>(type: T.Type, dependingOn dependency1: D1.Type, and dependency2: D2.Type, and dependency3: D3.Type, and dependency4: D4.Type, and dependency5: D5.Type, usingFactory factory: (D1, D2, D3, D4, D5) -> T) -> Loader {
        return loadWithType(type, usingLifecycle: DEFAULT_LIFECYCLE, dependingOn: dependency1, and: dependency2, and: dependency3, and: dependency4, and: dependency5, usingFactory: factory)
    }

    /**
        ...
    */
    public func loadWithType<T, D1, D2, D3, D4, D5>(type: T.Type, usingLifecycle lifecycle: Lifecycle, dependingOn dependency1: D1.Type, and dependency2: D2.Type, and dependency3: D3.Type, and dependency4: D4.Type, and dependency5: D5.Type, usingFactory factory: (D1, D2, D3, D4, D5) -> T) -> Loader {
        container.loadWithType(type, taggedAs: nil, usingLifecycle: lifecycle, dependingOn: [dependency1, dependency2, dependency3, dependency4, dependency5]) {
            let d1: D1? = self.container.instanceOfType(dependency1)
            let d2: D2? = self.container.instanceOfType(dependency2)
            let d3: D3? = self.container.instanceOfType(dependency3)
            let d4: D4? = self.container.instanceOfType(dependency4)
            let d5: D5? = self.container.instanceOfType(dependency5)
            return d1 != nil && d2 != nil && d3 != nil && d4 != nil && d5 != nil ? factory(d1!, d2!, d3!, d4!, d5!) : nil
        }
        return self
    }

    /**
        ...
    */
    public func loadWithType<T>(type: T.Type) -> LoadingWithoutDependency<T> {
        return LoadingWithoutDependency(loader: self, taggedType: (type,  nil), lifecycle: DEFAULT_LIFECYCLE)
    }

    /**
        ...
    */
    public func loadWithType<T>(type: T.Type, usingLifecycle lifecycle: Lifecycle) -> LoadingWithoutDependency<T> {
        return LoadingWithoutDependency(loader: self, taggedType: (type,  nil), lifecycle: lifecycle)
    }

    /**
        ...
    */
    public func loadWithType<T>(type: T.Type, taggedAs tag: String) -> LoadingWithoutDependency<T> {
        return LoadingWithoutDependency(loader: self, taggedType: (type, tag), lifecycle: DEFAULT_LIFECYCLE)
    }

    /**
        ...
    */
    public func loadWithType<T>(type: T.Type, taggedAs tag: CustomStringConvertible) -> LoadingWithoutDependency<T> {
        return LoadingWithoutDependency(loader: self, taggedType: (type, tag.description), lifecycle: DEFAULT_LIFECYCLE)
    }

    /**
        ...
    */
    public func loadWithType<T>(type: T.Type, taggedAs tag: String, usingLifecycle lifecycle: Lifecycle) -> LoadingWithoutDependency<T> {
        return LoadingWithoutDependency(loader: self, taggedType: (type, tag), lifecycle: lifecycle)
    }

    /**
        ...
    */
    public func loadWithType<T>(type: T.Type, taggedAs tag: CustomStringConvertible, usingLifecycle lifecycle: Lifecycle) -> LoadingWithoutDependency<T> {
        return LoadingWithoutDependency(loader: self, taggedType: (type, tag.description), lifecycle: lifecycle)
    }
}

/**
   ...
*/
public class LoadingWithoutDependency<T> {

    let loader: Loader
    let taggedType: (T.Type, String?)
    let lifecycle: Lifecycle

    init(loader: Loader, taggedType: (T.Type, String?), lifecycle: Lifecycle) {
        self.loader = loader
        self.taggedType = taggedType
        self.lifecycle = lifecycle
    }

    /**
        ...
    */
    public func usingFactory(factory: () -> T) -> Loader {
        loader.container.loadWithType(taggedType.0, taggedAs: taggedType.1, usingLifecycle: lifecycle, dependingOn: nil) {
            return factory()
        }
        return loader
    }

    /**
        ...
    */
    public func dependingOn<D>(type: D.Type) -> LoadingWithOneDependency<T, D> {
        return dependingOn((type,  nil))
    }

    /**
        ...
    */
    public func dependingOn<D>(type: D.Type, taggedAs tag: String) -> LoadingWithOneDependency<T, D> {
        return dependingOn((type, tag))
    }

    /**
        ...
    */
    public func dependingOn<D>(type: D.Type, taggedAs tag: CustomStringConvertible) -> LoadingWithOneDependency<T, D> {
        return dependingOn((type, tag.description))
    }

    /**
        ...
    */
    func dependingOn<D>(taggedType: (D.Type, String?)) -> LoadingWithOneDependency<T, D> {
        return LoadingWithOneDependency(loader: loader, taggedType: self.taggedType, lifecycle: lifecycle, dependency: taggedType)
    }
}

/**
    ...
*/
public class LoadingWithOneDependency<T, D> : LoadingWithoutDependency<T> {

    let taggedDependency: (D.Type, String?)

    init(loader: Loader, taggedType: (T.Type, String?), lifecycle: Lifecycle, dependency: (D.Type, String?)) {
        self.taggedDependency = dependency
        super.init(loader: loader, taggedType: taggedType, lifecycle: lifecycle)
    }

    /**
        ...
    */
    public func usingFactory(factory: (D) -> T) -> Loader {
        loader.container.loadWithType(taggedType.0, taggedAs: taggedType.1, usingLifecycle: lifecycle, dependingOn: [taggedDependency.0]) {
            let d: D? = self.loader.container.instanceOfType(self.taggedDependency.0, taggedAs: self.taggedDependency.1)
            return d != nil ? factory(d!) : nil
        }
        return loader
    }

    /**
        ...
    */
    public func dependingOn<D2>(type: D2.Type) -> LoadingWithTwoDependencies<T, D, D2> {
        return dependingOn((type,  nil))
    }

    /**
        ...
    */
    public func dependingOn<D2>(type: D2.Type, taggedAs tag: String) -> LoadingWithTwoDependencies<T, D, D2> {
        return dependingOn((type, tag))
    }

    /**
        ...
    */
    public func dependingOn<D2>(type: D2.Type, taggedAs tag: CustomStringConvertible) -> LoadingWithTwoDependencies<T, D, D2> {
        return dependingOn((type, tag.description))
    }

    /**
        ...
    */
    func dependingOn<D2>(taggedType: (D2.Type, String?)) -> LoadingWithTwoDependencies<T, D, D2> {
        return LoadingWithTwoDependencies(loader: loader, taggedType: self.taggedType, lifecycle: lifecycle, dependency1: taggedDependency, dependency2: taggedType)
    }
}

/**
    ...
*/
public class LoadingWithTwoDependencies<T, D1, D2> : LoadingWithOneDependency<T, D1> {

    let taggedDependency2: (D2.Type, String?)

    init(loader: Loader, taggedType: (T.Type, String?), lifecycle: Lifecycle, dependency1: (D1.Type, String?), dependency2: (D2.Type, String?)) {
        self.taggedDependency2 = dependency2
        super.init(loader: loader, taggedType: taggedType, lifecycle: lifecycle, dependency: dependency1)
    }

    /**
        ...
    */
    public func usingFactory(factory: (D1, D2) -> T) -> Loader {
        loader.container.loadWithType(taggedType.0, taggedAs: taggedType.1, usingLifecycle: lifecycle, dependingOn: [taggedDependency.0, taggedDependency2.0]) {
            let d1: D1? = self.loader.container.instanceOfType(self.taggedDependency.0, taggedAs: self.taggedDependency.1)
            let d2: D2? = self.loader.container.instanceOfType(self.taggedDependency2.0, taggedAs: self.taggedDependency2.1)
            return d1 != nil && d2 != nil ? factory(d1!, d2!) : nil
        }
        return loader
    }

    /**
        ...
    */
    public func dependingOn<D3>(type: D3.Type) -> LoadingWithThreeDependencies<T, D1, D2, D3> {
        return dependingOn((type, nil))
    }

    /**
        ...
    */
    public func dependingOn<D3>(type: D3.Type, taggedAs tag: String) -> LoadingWithThreeDependencies<T, D1, D2, D3> {
        return dependingOn((type, tag))
    }

    /**
        ...
    */
    public func dependingOn<D3>(type: D3.Type, taggedAs tag: CustomStringConvertible) -> LoadingWithThreeDependencies<T, D1, D2, D3> {
        return dependingOn((type, tag.description))
    }

    /**
        ...
    */
    func dependingOn<D3>(taggedType: (D3.Type, String?)) -> LoadingWithThreeDependencies<T, D1, D2, D3> {
        return LoadingWithThreeDependencies(loader: loader, taggedType: self.taggedType, lifecycle: lifecycle, dependency1: taggedDependency, dependency2: taggedDependency2, dependency3: taggedType)
    }
}

/**
    ...
*/
public class LoadingWithThreeDependencies<T, D1, D2, D3> : LoadingWithTwoDependencies<T, D1, D2> {

    let taggedDependency3: (D3.Type, String?)

    init(loader: Loader, taggedType: (T.Type, String?), lifecycle: Lifecycle, dependency1: (D1.Type, String?), dependency2: (D2.Type, String?), dependency3: (D3.Type, String?)) {
        self.taggedDependency3 = dependency3
        super.init(loader: loader, taggedType: taggedType, lifecycle: lifecycle, dependency1: dependency1, dependency2: dependency2)
    }

    /**
        ...
    */
    public func usingFactory(factory: (D1, D2, D3) -> T) -> Loader {
        loader.container.loadWithType(taggedType.0, taggedAs: taggedType.1, usingLifecycle: lifecycle, dependingOn: [taggedDependency.0, taggedDependency2.0, taggedDependency3.0]) {
            let d1: D1? = self.loader.container.instanceOfType(self.taggedDependency.0, taggedAs: self.taggedDependency.1)
            let d2: D2? = self.loader.container.instanceOfType(self.taggedDependency2.0, taggedAs: self.taggedDependency2.1)
            let d3: D3? = self.loader.container.instanceOfType(self.taggedDependency3.0, taggedAs: self.taggedDependency3.1)
            return d1 != nil && d2 != nil && d3 != nil ? factory(d1!, d2!, d3!) : nil
        }
        return loader
    }

    /**
        ...
    */
    public func dependingOn<D4>(type: D4.Type) -> LoadingWithFourDependencies<T, D1, D2, D3, D4> {
        return dependingOn((type,  nil))
    }

    /**
        ...
    */
    public func dependingOn<D4>(type: D4.Type, taggedAs tag: String) -> LoadingWithFourDependencies<T, D1, D2, D3, D4> {
        return dependingOn((type, tag))
    }

    /**
        ...
    */
    public func dependingOn<D4>(type: D4.Type, taggedAs tag: CustomStringConvertible) -> LoadingWithFourDependencies<T, D1, D2, D3, D4> {
        return dependingOn((type, tag.description))
    }

    /**
        ...
    */
    func dependingOn<D4>(taggedType: (D4.Type, String?)) -> LoadingWithFourDependencies<T, D1, D2, D3, D4> {
        return LoadingWithFourDependencies(loader: loader, taggedType: self.taggedType, lifecycle: lifecycle, dependency1: taggedDependency, dependency2: taggedDependency2, dependency3: taggedDependency3, dependency4: taggedType)
    }
}

/**
    ...
*/
public class LoadingWithFourDependencies<T, D1, D2, D3, D4> : LoadingWithThreeDependencies<T, D1, D2, D3> {

    let taggedDependency4: (D4.Type, String?)

    init(loader: Loader, taggedType: (T.Type, String?), lifecycle: Lifecycle, dependency1: (D1.Type, String?), dependency2: (D2.Type, String?), dependency3: (D3.Type, String?), dependency4: (D4.Type, String?)) {
        self.taggedDependency4 = dependency4
        super.init(loader: loader, taggedType: taggedType, lifecycle: lifecycle, dependency1: dependency1, dependency2: dependency2, dependency3: dependency3)
    }

    /**
        ...
    */
    public func usingFactory(factory: (D1, D2, D3, D4) -> T) -> Loader {
        loader.container.loadWithType(taggedType.0, taggedAs: taggedType.1, usingLifecycle: lifecycle, dependingOn: [taggedDependency.0, taggedDependency2.0, taggedDependency3.0, taggedDependency4.0]) {
            let d1: D1? = self.loader.container.instanceOfType(self.taggedDependency.0, taggedAs: self.taggedDependency.1)
            let d2: D2? = self.loader.container.instanceOfType(self.taggedDependency2.0, taggedAs: self.taggedDependency2.1)
            let d3: D3? = self.loader.container.instanceOfType(self.taggedDependency3.0, taggedAs: self.taggedDependency3.1)
            let d4: D4? = self.loader.container.instanceOfType(self.taggedDependency4.0, taggedAs: self.taggedDependency4.1)
            return d1 != nil && d2 != nil && d3 != nil && d4 != nil ? factory(d1!, d2!, d3!, d4!) : nil
        }
        return loader
    }

    /**
        ...
    */
    public func dependingOn<D5>(type: D5.Type) -> LoadingWithFiveDependencies<T, D1, D2, D3, D4, D5> {
        return dependingOn((type, nil))
    }

    /**
        ...
    */
    public func dependingOn<D5>(type: D5.Type, taggedAs tag: String) -> LoadingWithFiveDependencies<T, D1, D2, D3, D4, D5> {
        return dependingOn((type, tag))
    }

    /**
        ...
    */
    public func dependingOn<D5>(type: D5.Type, taggedAs tag: CustomStringConvertible) -> LoadingWithFiveDependencies<T, D1, D2, D3, D4, D5> {
        return dependingOn((type, tag.description))
    }

    /**
        ...
    */
    func dependingOn<D5>(taggedType: (D5.Type, String?)) -> LoadingWithFiveDependencies<T, D1, D2, D3, D4, D5> {
        return LoadingWithFiveDependencies(loader: loader, taggedType: self.taggedType, lifecycle: lifecycle, dependency1: taggedDependency, dependency2: taggedDependency2, dependency3: taggedDependency3, dependency4: taggedDependency4, dependency5: taggedType)
    }
}

/**
    ...
*/
public class LoadingWithFiveDependencies<T, D1, D2, D3, D4, D5> : LoadingWithFourDependencies<T, D1, D2, D3, D4> {

    let taggedDependency5: (D5.Type, String?)

    init(loader: Loader, taggedType: (T.Type, String?), lifecycle: Lifecycle, dependency1: (D1.Type, String?), dependency2: (D2.Type, String?), dependency3: (D3.Type, String?), dependency4: (D4.Type, String?), dependency5: (D5.Type, String?)) {
        self.taggedDependency5 = dependency5
        super.init(loader: loader, taggedType: taggedType, lifecycle: lifecycle, dependency1: dependency1, dependency2: dependency2, dependency3: dependency3, dependency4: dependency4)
    }

    /**
        ...
    */
    public func usingFactory(factory: (D1, D2, D3, D4, D5) -> T) -> Loader {
        loader.container.loadWithType(taggedType.0, taggedAs: taggedType.1, usingLifecycle: lifecycle, dependingOn: [taggedDependency.0, taggedDependency2.0, taggedDependency3.0, taggedDependency4.0, taggedDependency5.0]) {
            let d1: D1? = self.loader.container.instanceOfType(self.taggedDependency.0, taggedAs: self.taggedDependency.1)
            let d2: D2? = self.loader.container.instanceOfType(self.taggedDependency2.0, taggedAs: self.taggedDependency2.1)
            let d3: D3? = self.loader.container.instanceOfType(self.taggedDependency3.0, taggedAs: self.taggedDependency3.1)
            let d4: D4? = self.loader.container.instanceOfType(self.taggedDependency4.0, taggedAs: self.taggedDependency4.1)
            let d5: D5? = self.loader.container.instanceOfType(self.taggedDependency5.0, taggedAs: self.taggedDependency5.1)
            return d1 != nil && d2 != nil && d3 != nil && d4 != nil && d5 != nil ? factory(d1!, d2!, d3!, d4!, d5!) : nil
        }
        return loader
    }
}

