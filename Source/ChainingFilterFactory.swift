/**
    ...
*/
class ChainingFilterFactory : FilterFactory {

    class FilterChain : Filter {

        var filters: [Filter] = []

        init(filters: [Filter]) {
            self.filters += filters
        }

        func applyToFactory(factory: () -> Any?) -> () -> Any? {
            return filters.reduce(factory) { $1.applyToFactory($0) }
        }
    }

    var factories: [FilterFactory] = []

    init(factories: FilterFactory...) {
        self.factories += factories
    }

    func filterConfiguredWithType(type: Any.Type, tag: String?, lifecycle: Lifecycle, dependencies: [Any.Type]?) -> Filter {
        return FilterChain(filters: factories.map({
            $0.filterConfiguredWithType(type, tag: tag, lifecycle: lifecycle, dependencies: dependencies)
        }))
    }
}