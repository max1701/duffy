/**
    ...
*/
protocol Filter {

    /**
        ...
    */
    func applyToFactory(factory: () -> Any?) -> () -> Any?
}