
import XCTest
import Duffy

class LifecycleTests : XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testTransientInstantiatingWithoutDependency() {
        let container = Duffy.newContainer.loadedBy {
            $0.loadWithType(TransactionLog.self, usingLifecycle: Lifecycle.Transient) { return TransactionLog() }
        }

        let instance1: TransactionLog? = container.instanceOfType(TransactionLog.self)
        let instance2: TransactionLog? = container.instanceOfType(TransactionLog.self)

        XCTAssertNotNil(instance1)
        XCTAssertNotNil(instance2)
        XCTAssertTrue(instance1! !== instance2!)
    }

    func testSingletonInstantiatingWithoutDependency() {
        let container = Duffy.newContainer.loadedBy {
            $0.loadWithType(TransactionLog.self, usingLifecycle: Lifecycle.Singleton) { return TransactionLog() }
        }

        let instance1: TransactionLog? = container.instanceOfType(TransactionLog.self)
        let instance2: TransactionLog? = container.instanceOfType(TransactionLog.self)

        XCTAssertNotNil(instance1)
        XCTAssertNotNil(instance2)
        XCTAssertTrue(instance1! === instance2!)
    }


    func testTransientInstantiatingWithOneDependency() {
        let container = Duffy.newContainer.loadedBy {
            loader in
            loader.loadWithType(CreditCardProcessor.self, usingLifecycle: Lifecycle.Transient, dependingOn: TransactionLog.self) {
                txLog in return DefaultCreditCardProcessorImpl(transactionLog: txLog)
            }
            loader.loadWithType(TransactionLog.self, usingLifecycle: Lifecycle.Transient) { return TransactionLog() }
        }

        let instance1: CreditCardProcessor? = container.instanceOfType(CreditCardProcessor.self)
        let instance2: CreditCardProcessor? = container.instanceOfType(CreditCardProcessor.self)

        XCTAssertNotNil(instance1)
        XCTAssertNotNil(instance2)
        XCTAssertTrue(instance1! !== instance2!)

        XCTAssertNotNil((instance1 as! DefaultCreditCardProcessorImpl).transactionLog)
        XCTAssertNotNil((instance2 as! DefaultCreditCardProcessorImpl).transactionLog)
        XCTAssertTrue((instance1 as! DefaultCreditCardProcessorImpl).transactionLog! !== (instance2 as! DefaultCreditCardProcessorImpl).transactionLog!)
    }

    func testSingletonInstantiatingWithOneDependency() {
        let container = Duffy.newContainer.loadedBy {
            $0.loadWithType(CreditCardProcessor.self, usingLifecycle: Lifecycle.Singleton, dependingOn: TransactionLog.self) {
                return DefaultCreditCardProcessorImpl(transactionLog: $0)
            }
            $0.loadWithType(TransactionLog.self) { return TransactionLog() }
        }

        let instance1: CreditCardProcessor? = container.instanceOfType(CreditCardProcessor.self)
        let instance2: CreditCardProcessor? = container.instanceOfType(CreditCardProcessor.self)

        XCTAssertNotNil(instance1)
        XCTAssertNotNil(instance2)
        XCTAssertTrue(instance1! === instance2!)
    }

}
