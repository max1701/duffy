
import XCTest
import Duffy

class InjectionTests : XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testShortcutLoadingAndInstantiating_NoDependencies() {
        let container = Duffy.newContainer.loadedBy {
            loader in loader.loadWithType(TransactionLog.self) { return TransactionLog() }
        }

        let instance: TransactionLog? = container.instanceOfType(TransactionLog.self)
        XCTAssertNotNil(instance)
        XCTAssertNil(instance?.level)
    }

    func testUntaggedTypeLoadingAndInstantiating_NoDependencies() {
        let container = Duffy.newContainer.loadedBy {
            loader in loader.loadWithType(TransactionLog.self).usingFactory { return TransactionLog() }
        }

        let instance: TransactionLog? = container.instanceOfType(TransactionLog.self)
        XCTAssertNotNil(instance)
        XCTAssertNil(instance?.level)
    }

    func testTaggedTypeLoadingAndInstantiating_NoDependencies() {
        let tag = "aTag"
        let container = Duffy.newContainer.loadedBy {
            loader in loader.loadWithType(TransactionLog.self, taggedAs: tag).usingFactory { return TransactionLog() }
        }

        let instance: TransactionLog? = container.instanceOfType(TransactionLog.self, taggedAs: tag)
        XCTAssertNotNil(instance)
        XCTAssertNil(instance?.level)

        let unknown: TransactionLog? = container.instanceOfType(TransactionLog.self, taggedAs: "unknownTag")
        XCTAssertNil(unknown)
    }

    func testTaggedTypeLoadingAndInstantiationFallback() {
        let tag = "aTag"
        let container = Duffy.newContainer.loadedBy {
            loader in loader.loadWithType(TransactionLog.self, taggedAs: tag).usingFactory { return TransactionLog() }
        }

        let instance: TransactionLog? = container.instanceOfType(TransactionLog.self)
        XCTAssertNotNil(instance)
        XCTAssertNil(instance?.level)
    }

    func testShortcutTypeLoadingAndInstantiating_OneDependency() {
        let container = Duffy.newContainer.loadedBy {
            loader in
            loader.loadWithType(CreditCardProcessor.self, dependingOn: TransactionLog.self) { return DefaultCreditCardProcessorImpl(transactionLog: $0) }
            loader.loadWithType(TransactionLog.self) { return TransactionLog() }
        }

        let instance: CreditCardProcessor? = container.instanceOfType(CreditCardProcessor.self)
        XCTAssertNotNil(instance)
        XCTAssertTrue(instance! is DefaultCreditCardProcessorImpl)
        XCTAssertNotNil((instance as! DefaultCreditCardProcessorImpl).transactionLog)
    }

    func testUntaggedTypeLoadingAndInstantiating_OneDependency() {
        let container = Duffy.newContainer.loadedBy {
            loader in loader.loadWithType(TransactionLog.self).usingFactory { return TransactionLog() }
        }

        let instance: TransactionLog? = container.instanceOfType(TransactionLog.self)
        XCTAssertNotNil(instance)
        XCTAssertNil(instance?.level)
    }

    func testShortcutTypeLoadingAndInstantiating_TwoDependencies() {
        let container = Duffy.newContainer.loadedBy {
            loader in
            loader.loadWithType(BillingService.self, dependingOn: CreditCardProcessor.self, and: TransactionLog.self) {
                let s = BillingService(creditCardProcessor: $0)
                s.transactionLog = $1
                return s
            }
            loader.loadWithType(CreditCardProcessor.self, dependingOn: TransactionLog.self) { return DefaultCreditCardProcessorImpl(transactionLog: $0) }
            loader.loadWithType(TransactionLog.self) { return TransactionLog() }
        }

        let instance: BillingService? = container.instanceOfType(BillingService.self)
        XCTAssertNotNil(instance)
        XCTAssertTrue(instance!.creditCardProcessor is DefaultCreditCardProcessorImpl)
        XCTAssertNotNil(instance!.transactionLog)
    }

    func testUntaggedTypeLoadingAndInstantiating_TwoDependencies() {
        let container = Duffy.newContainer.loadedBy {
            loader in
            loader.loadWithType(BillingService.self).dependingOn(CreditCardProcessor.self).dependingOn(TransactionLog.self).usingFactory {
                let s = BillingService(creditCardProcessor: $0)
                s.transactionLog = $1
                return s
            }
            loader.loadWithType(CreditCardProcessor.self).dependingOn(TransactionLog.self).usingFactory { return DefaultCreditCardProcessorImpl(transactionLog: $0) }
            loader.loadWithType(TransactionLog.self).usingFactory { return TransactionLog() }
        }

        let instance: BillingService? = container.instanceOfType(BillingService.self)
        XCTAssertNotNil(instance)
        XCTAssertTrue(instance!.creditCardProcessor is DefaultCreditCardProcessorImpl)
        XCTAssertNotNil(instance!.transactionLog)
    }

    func testTaggedTypeLoadingAndInstantiating_OneDependency() {

        enum Tags : String, CustomStringConvertible {
            case LOG = "Log"
            case PROCESSOR = "Processor"
            var description: String { return self.rawValue }
        }

        let container = Duffy.newContainer.loadedBy {
            loader in

            loader
                .loadWithType(CreditCardProcessor.self, taggedAs: Tags.PROCESSOR)
                .dependingOn(TransactionLog.self, taggedAs: Tags.LOG)
                .usingFactory { return DefaultCreditCardProcessorImpl(transactionLog: $0) }

            loader.loadWithType(TransactionLog.self, taggedAs: Tags.LOG).usingFactory { return TransactionLog() }
        }

        let instance: CreditCardProcessor? = container.instanceOfType(CreditCardProcessor.self, taggedAs: Tags.PROCESSOR)
        XCTAssertNotNil(instance)
        XCTAssertTrue(instance! is DefaultCreditCardProcessorImpl)
        XCTAssertNotNil((instance as! DefaultCreditCardProcessorImpl).transactionLog)
    }

    // ...

    func testUntaggedTypeLoadingAndInstantiating_ThreeDependencies() {
        let container = Duffy.newContainer.loadedBy {
            loader in
            loader
                .loadWithType(Multiply3Integers.self, usingLifecycle: Lifecycle.Transient)
                .dependingOn(Int.self, taggedAs: "1st")
                .dependingOn(Int.self, taggedAs: "2nd")
                .dependingOn(Int.self, taggedAs: "3rd")
                .usingFactory {
                    return Multiply3IntegersImpl(int1: $0, int2: $1, int3: $2)
                }
            loader.loadWithType(Int.self, taggedAs: "1st").usingFactory { 2 }
            loader.loadWithType(Int.self, taggedAs: "2nd").usingFactory { 3 }
            loader.loadWithType(Int.self, taggedAs: "3rd").usingFactory { 5 }
        }

        let instance: Multiply3Integers? = container.instanceOfType(Multiply3Integers.self)
        XCTAssertTrue(instance != nil)
        XCTAssertEqual(2 * 3 * 5, instance!.multiply())
    }

    func testUntaggedTypeLoadingAndInstantiating_FiveDependencies() {
        let container = Duffy.newContainer.loadedBy {
            loader in
            loader
                .loadWithType(Multiply5Integers.self, usingLifecycle: Lifecycle.Transient)
                .dependingOn(Int.self, taggedAs: "1st")
                .dependingOn(Int.self, taggedAs: "2nd")
                .dependingOn(Int.self, taggedAs: "3rd")
                .dependingOn(Int.self, taggedAs: "4th")
                .dependingOn(Int.self, taggedAs: "5th")
                .usingFactory {
                    return Multiply5IntegersImpl(int1: $0, int2: $1, int3: $2, int4: $3, int5: $4)
                }
            loader.loadWithType(Int.self, taggedAs: "1st").usingFactory { 2 }
            loader.loadWithType(Int.self, taggedAs: "2nd").usingFactory { 3 }
            loader.loadWithType(Int.self, taggedAs: "3rd").usingFactory { 5 }
            loader.loadWithType(Int.self, taggedAs: "4th").usingFactory { 7 }
            loader.loadWithType(Int.self, taggedAs: "5th").usingFactory { 11 }
        }

        let instance: Multiply5Integers? = container.instanceOfType(Multiply5Integers.self)
        XCTAssertTrue(instance != nil)
        XCTAssertEqual(2 * 3 * 5 * 7 * 11, instance!.multiply())
    }

}
