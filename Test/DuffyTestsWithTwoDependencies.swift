
import XCTest
import Duffy

class DuffyTestsWithTwoDependencies : XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

//    func testTaggedShortcutClassLoadingAndInstantiationFallback() {
//        let tag = "aTag"
//        let container = Duffy.newContainer.loadedBy {
//            $0.loadWithType(CreditCardProcessor.self, taggedAs: tag, dependingOn: TransactionLog.self) { return DefaultCreditCardProcessorImpl(transactionLog: $0) }
//            $0.loadWithType(TransactionLog.self) { return TransactionLog() }
//        }
//
//        let instance: CreditCardProcessor? = container.instanceOfType(CreditCardProcessor.self)
//        XCTAssertNotNil(instance)
//        XCTAssertTrue(instance! is DefaultCreditCardProcessorImpl)
//        XCTAssertNotNil((instance as! DefaultCreditCardProcessorImpl).transactionLog)
//    }

//
//    func testTaggedShortcutClassLoadingAndInstantiatingUsingATaggedType() {
//        let tag = "aTag"
//        let container = Duffy.newContainer.loadedBy {
//            $0.loadWithType((TransactionLog.self, tag)) { return TransactionLog() }
//        }
//
//        let instance: TransactionLog? = container.instanceOfType(TransactionLog.self, taggedAs: tag)
//        XCTAssertNotNil(instance)
//        XCTAssertNil(instance?.level)
//
//        let unknown: TransactionLog? = container.instanceOfType(TransactionLog.self, taggedAs: "unknownTag")
//        XCTAssertNil(unknown)
//    }
//
//    func testUntaggedClassLoadingAndInstantiating() {
//        let container = Duffy.newContainer.loadedBy {
//            $0.loadType(TransactionLog.self).usingFactory { return TransactionLog() }
//        }
//
//        let instance: TransactionLog? = container.instanceOfType(TransactionLog.self)
//        XCTAssertNotNil(instance)
//        XCTAssertNil(instance?.level)
//    }
//
//    func testTaggedClassLoadingAndInstantiating() {
//        let tag = "aTag"
//        let container = Duffy.newContainer.loadedBy {
//            $0.loadType(TransactionLog.self, taggedAs: tag).usingFactory { return TransactionLog() }
//        }
//
//        let instance: TransactionLog? = container.instanceOfType(TransactionLog.self, taggedAs: tag)
//        XCTAssertNotNil(instance)
//        XCTAssertNil(instance?.level)
//
//        let unknown: TransactionLog? = container.instanceOfType(TransactionLog.self, taggedAs: "unknownTag")
//        XCTAssertNil(unknown)
//    }
//
//    func testTaggedClassLoadingAndInstantiatingUsingATaggedType() {
//        let tag = "aTag"
//        let container = Duffy.newContainer.loadedBy {
//            $0.loadType((TransactionLog.self, tag)).usingFactory { return TransactionLog() }
//        }
//
//        let instance: TransactionLog? = container.instanceOfType(TransactionLog.self, taggedAs: tag)
//        XCTAssertNotNil(instance)
//        XCTAssertNil(instance?.level)
//
//        let unknown: TransactionLog? = container.instanceOfType(TransactionLog.self, taggedAs: "unknownTag")
//        XCTAssertNil(unknown)
//    }

//    func testClassRegistrationWithoutDependencyUsingAlternativeAPI() {
//        let container = Container()
//        do {
//
//            container
//                .register(TransactionLog.self, withLifecycle: .Transient)
//                .usingFactory {
//                    () -> TransactionLog in
//                    return TransactionLog()
//                }
//
//            let instance: TransactionLog = try container.getInstanceOfType(TransactionLog.self)
//
//            XCTAssertNil(instance.level)
//
//        } catch {
//            XCTFail("failed due to a thrown error")
//        }
//    }

}
