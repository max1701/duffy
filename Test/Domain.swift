//
// Created by ms on 05/08/15.
// Copyright (c) 2015 ms. All rights reserved.
//

class BillingService {

    var transactionLog: TransactionLog?
    var creditCardProcessor: CreditCardProcessor?

    init(creditCardProcessor: CreditCardProcessor?) {
        self.creditCardProcessor = creditCardProcessor
    }
}

class TransactionLog {

    var level: Int?

    init(level: Int?) {
        self.level = level
    }

    convenience init() {
        self.init(level: nil)
    }
}

protocol CreditCardProcessor : class {
}

class DefaultCreditCardProcessorImpl : CreditCardProcessor {
    var transactionLog: TransactionLog?

    init(transactionLog: TransactionLog?) {
        self.transactionLog = transactionLog
    }

    convenience init() {
        self.init(transactionLog: nil)
    }
}


protocol Multiply3Integers {
    func multiply() -> Int
}

class Multiply3IntegersImpl: Multiply3Integers {

    let int1: Int
    let int2: Int
    let int3: Int

    init(int1: Int, int2: Int, int3: Int) {
        self.int1 = int1
        self.int2 = int2
        self.int3 = int3
    }

    func multiply() -> Int {
        return int1 * int2 * int3
    }
}


protocol Multiply5Integers {
    func multiply() -> Int
}

class Multiply5IntegersImpl: Multiply5Integers {

    let int1: Int
    let int2: Int
    let int3: Int
    let int4: Int
    let int5: Int

    init(int1: Int, int2: Int, int3: Int, int4: Int, int5: Int) {
        self.int1 = int1
        self.int2 = int2
        self.int3 = int3
        self.int4 = int4
        self.int5 = int5
    }

    func multiply() -> Int {
        return int1 * int2 * int3 * int4 * int5
    }
}


class Algorithm {

    let calculation: (Int, Int) -> Int

    init(calculation: (Int, Int) -> Int) {
        self.calculation = calculation
    }

    func calculate() -> Int {
        return 2 * calculation(3, 4)
    }

}


