
import XCTest
import Duffy

class FunctionTypesInjectionTests : XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testTypeLoadingAndInstantiating_FunctionTypeDependency() {

        typealias FunctionType = (Int, Int) -> Int

        let container = Duffy.newContainer.loadedBy {
            loader in
            loader.loadWithType(Algorithm.self).dependingOn(FunctionType.self).usingFactory {
                f in return Algorithm(calculation: f)
            }
            loader.loadWithType(FunctionType.self) {
                return {
                    (a: Int, b: Int) -> Int in
                    a * b
                }
            }
        }

        let instance: Algorithm? = container.instanceOfType(Algorithm.self)
        XCTAssertNotNil(instance)
        XCTAssertEqual(24, instance!.calculate())
    }

}
