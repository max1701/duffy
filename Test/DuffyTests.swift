import XCTest
import Duffy

class DuffyTests : XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testShortcutLoadingAndInstantiatingOfClassTypeWithNoDependencies() {
        let VALUE =  1701
        let container = Duffy.newContainer.loadedBy { loader in
            loader.loadWithType(Int.self) { return VALUE }
        }
        let instance: Int? = container.instanceOfType(Int.self)
        XCTAssertNotNil(instance)
        XCTAssertEqual(VALUE, instance)
    }

}
